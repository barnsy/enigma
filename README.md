# Enigma machine written in C for one of my university assignments.

Flow of the encoding is as follows.
Input -> Plugboard -> Rotor III -> Rotor II -> Rotor I -> Reflector -> Rotor I -> Rotor II -> Rotor
III -> Plugboard -> Output

The machine uses these rotors.  
Rotor I = EKMFLGDQVZNTOWYHXUSPAIBRCJ  
Rotor II = AJDKSIRUXBLHWTMCQGZNPYFVOE  
Rotor III = BDFHJLCPRTXVZNYEIWGAKMUSQO   

Rotor turnover positions used = QEVJZ

Reflector used = YRUHQSLDPXNGOKMIEBFZCWVJAT

Plugboard settings = ZPHNMSWCIYTQEDOBLRFKUVGXJA

Future upgrades could have the following features.
- Better input sanitisation.
- Option for users to select which rotors to use and in which position.
- Option for users to select which reflector to use. 
- Have input message and output message read and write to files respectively.  
