#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

char encryption_forward(char letter, char* encrypt_table);
char encryption_reverse(char letter, char* encrypt_table);
void set_plugboard_pair(unsigned char char_index, unsigned char pair_index);
void rotate_array(char input[]);

/* Char arrays for alphabet and default plugboard */
char alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
char plugboard[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

/* Rotor substitution table */
char rotorI[] = "EKMFLGDQVZNTOWYHXUSPAIBRCJ";
char rotorII[] = "AJDKSIRUXBLHWTMCQGZNPYFVOE";
char rotorIII[] = "BDFHJLCPRTXVZNYEIWGAKMUSQO";
char rotorIV[] = "ESOVPZJAYQUIRHXLNFTGKDCMWB";
char rotorV[] = "VZBRGITYUPSDNHLXAWMJQIFECK";

/* Rotor turover positions as per documentation */
char rotor_turnover_pos[] ="QEVJZ";

/* Reflector substitution table */
char reflectorB[] = "EJMZALYXVBWFCRQUONTSPIKHGD";
char reflectorC[] = "YRUHQSLDPXNGOKMIEBFZCWVJAT";


int main(void)
{
    /* Text input to encrypt/decrypt */
    char text_input[4096];
    printf("Enter text to be encrypted with no spaces: \n");
    scanf("%s", text_input);

    /* Check to ensure characters are alphanumeric only */
    for(int i = 0; i < strlen(text_input); i++)
    {
        if (!isalpha(text_input[i]))
        {
            printf("Input can only be alphanumerical characters!");
            exit(0);
        }
    }

    char text_encryption[] = "\0";

    /* Rotor counters */
    int r1 = 0;
    int r2 = 0;
    int r3 = 0;

    /* Rotor offsets */
    unsigned int rotor1_offset = 0;
    unsigned int rotor2_offset = 0;
    unsigned int rotor3_offset = 0;

    /* Rotor offset positions */
    printf("Enter starting positions of rotors with numbers less than 25\n");
    printf("For example \"001\" starts rotors at \"AAB\"\n");
    scanf("%d, %d, %d", &rotor3_offset, &rotor2_offset, &rotor1_offset);

    // Check to make sure input offsets are within correct range.
    if(rotor3_offset > 25 || rotor2_offset > 25 || rotor1_offset > 25 )
    {
        printf("Offset out of bounds.");
        exit(0);
    }

    // Rotor counters plus user counter offset.
    r1 = r1 + rotor1_offset;
    r2 = r2 + rotor2_offset;
    r3 = r3 + rotor3_offset;

    // Counter used to print correct encrypted text length.
    int input_size = 0;

    /* Plugboard connections as per lecture slides */
    set_plugboard_pair('Z', 'A');    // ZA
    set_plugboard_pair('P', 'B');    // PB
    set_plugboard_pair('H', 'C');    // HC
    set_plugboard_pair('N', 'D');    // ND
    set_plugboard_pair('M', 'E');    // ME
    set_plugboard_pair('S', 'F');    // SF
    set_plugboard_pair('W', 'G');    // WG
    set_plugboard_pair('Y', 'J');    // YJ
    set_plugboard_pair('T', 'K');    // TK
    set_plugboard_pair('Q', 'L');    // QL

    printf("Alphabet: \n%s\n\n", alphabet);               // Prints out the alphabet
    printf("Plugboard settings: \n%s\n\n", plugboard);    // Prints out the plugboard
    printf("RotorIII settings: \n%s\n\n", rotorIII);      // Prints out rotor III
    printf("RotorII settings: \n%s\n\n", rotorII);        // Prints out rotor II
    printf("RotorI settings: \n%s\n\n", rotorI);          // Prints out rotor I
    printf("Reflector settings: \n%s\n\n", reflectorC);   // Prints out reflector C
    printf("Original text: %s\n", text_input);            // Prints out the original text

    // Input char set to A as a default value.
    char input_char = 'A';

    for (int i = 0; i < strlen(text_input); i++)
    {
        // Rotation checks for turnover positions.
        if (alphabet[r3] == rotor_turnover_pos[2])
        {
            r2++;
            rotate_array(rotorII);
        }
        if (r1 == 25)
        {
            r1 = -1;
        }
        if (r2 == 25)
        {
            r2 = -1;
        }
        if (r3 == 25)
        {
            r3 = -1;
        }
        r3++;
        rotate_array(rotorIII);

        /* Ensure all text is capitalized as per original enigma machine */
        text_input[i] = toupper(text_input[i]);

        /* Print out current rotor position and input character */
        printf("\n\n %c, %c, %c\n", alphabet[r1], alphabet[r2], alphabet[r3]);
        printf(" %d, %d, %d\n", r1, r2, r3);
        printf("\n Input char = %c\n", text_input[i]);

        /* Forward path: Plugboard, Rotor1, Rotor2, Rotor3, Reflector */
        /* Plugboard encryption */
        input_char = encryption_forward(text_input[i], plugboard);
        printf("\n Encrypted character - plugboard = %c",input_char);

        /* RotorIII encryption */
        input_char = encryption_forward(input_char, rotorIII);
        input_char = encryption_forward('A' + ((input_char - r3) + 'A') % 26, alphabet);
        printf("\n Encrypted character - rotorIII = %c", input_char);

        /* RotorII encryption */
        input_char = encryption_forward(input_char, rotorII);
        input_char = encryption_forward('A' + ((input_char - r2 ) + 'A') % 26, alphabet);
        printf("\n Encrypted character - rotorII = %c", input_char);

        /* RotorI encryption */
        input_char = encryption_forward(input_char, rotorI);
        input_char = encryption_forward('A' + ((input_char - r1) + 'A') % 26, alphabet);
        printf("\n Encrypted character - rotorI = %c", input_char);

        /* Reflector encryption */
        input_char = encryption_forward('A' + (input_char + 'A') % 26, reflectorC);
        printf("\n Encrypted character - reflector C = %c", input_char);
        printf("\n Encrypted character of the FORWARD path is = %c \n", input_char);

        /* Reverse path: Rotor3, Rotor2, Rotor1, Plugboard */
        // No rotation in the reverse path
        printf("\n Now is the REVERSE PATH");

        /* RotorI inverse encryption */
        input_char = encryption_reverse('A' + ((input_char + r1) + 'A') % 26, rotorI);
        printf("\n Encrypted character - rotorI = %c", input_char);

        /* RotorII inverse encryption */
        input_char = encryption_reverse('A' + ((input_char + r2) + 'A') % 26,  rotorII);
        printf("\n Encrypted character - rotorII = %c", input_char);

        /* RotorIII inverse encryption */
        input_char = encryption_reverse('A' + ((input_char + r3) + 'A') % 26, rotorIII);
        printf("\n Encrypted character - rotorIII = %c", input_char);

        /* Plugboard inverse encryption */
        input_char = encryption_reverse(input_char, plugboard);
        printf("\n Encrypted character - plugboard = %c",input_char);

        // Further rotor checks if for turnover positions.
        if (alphabet[r2] == rotor_turnover_pos[1])
        {
            r1++;
            r2++;
            rotate_array(rotorII);
            rotate_array(rotorI);
        }
        // Increment size counter to ensure printing of encrypted string is correct length.
        input_size++;
        text_encryption[i] = input_char;
    }

    printf("\nAfter plugboard encryption: ");
    for(int i = 0; i < input_size;i++)
    {
        printf("%c", text_encryption[i]);
    }

    printf("\n");
    return 0;

}


/* Functions */
/* Definition of the function encryption_forward */
char encryption_forward(char letter, char* encrypt_table)
{
    int i = 0;
    // Loops through length of encryption table.
	for(i = 0; i < strlen(encrypt_table); i++)
	{
        // If letter matches letter in index of alphabet.
	    if(letter == alphabet[i])
	    {
            // Returns character at index of table being used.
		    return encrypt_table[i];
	    }
	}
    // Returns char '0' if encryption char is out of bounds.
    return '0';
}

/* Definition of the function encryption_reverse */
char encryption_reverse(char letter, char* encrypt_table)
{
    int i = 0;
	for(i = 0; i < strlen(encrypt_table); i++)
	{
	    if(letter == encrypt_table[i])
	    {
        	return alphabet[i];
	    }
	}
    return '0';
}

/* Set plugboard pairs */
void set_plugboard_pair(unsigned char char_index, unsigned char pair_index)
{
    // Check to make sure chars are part of the alphabet.
    if (!isalpha(char_index) || !isalpha(pair_index))
    {
        printf("Your plugboard settings are incorrect");
    }

    // Ensure characters within alphabet range.
    char_index = char_index - 'A';
    pair_index = pair_index - 'A';

    plugboard[pair_index] = alphabet[char_index];
    plugboard[char_index] = alphabet[pair_index];
}

/* Rotate rotor arrays */
void rotate_array(char input[]) {
  int i, first_char;

  // Store first character from the input array.
  first_char = input[0];

  // Shift array positions.
  for (i = 0; i < strlen(input); i++)
  {
    input[i] = input[i + 1];
  }

  // Add first character to end of array.
  input[strlen(input)] = first_char;
}

